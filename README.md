
# For Developers

Install virtualenv

```python -m pip install --user virtualenv```

Create a virtualenv

```python -m virtualenv env```

Before you can start installing or using packages in your virtualenv you’ll need to activate it.

```source env/bin/activate``` or On Windows: ```.\env\Scripts\activate.bat```

Installing packages

```pip install -r requirements.txt```

If you want to switch projects or otherwise leave your virtualenv, simply run:

```deactivate```




# TODO
- [x] repository & python setup
- [x] argparse
- [x] sqlite
- [x] ratings parser
- [x] rss file
- [x] arrange feed fields
- [x] server setup (caddy)
- [x] server setup (python)
- [x] server setup (cron)
