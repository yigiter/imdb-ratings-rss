import logging
import argparse
import requests
import sqlite3
from datetime import datetime
import pathlib
from bs4 import BeautifulSoup
from urllib.parse import urlparse, parse_qs
from feedgen.feed import FeedGenerator

_CREATE_TABLE_MOVIE_ = "CREATE TABLE IF NOT EXISTS movie(movie_id TEXT, name TEXT, year TEXT, runtime TEXT, genre TEXT, rating TEXT, image_link TEXT, PRIMARY KEY(movie_id));"
_CREATE_TABLE_USER_ = "CREATE TABLE IF NOT EXISTS user(user_id TEXT, name TEXT, PRIMARY KEY(user_id));"
_CREATE_TABLE_USER_RATINGS_ = "CREATE TABLE IF NOT EXISTS user_ratings(user_id TEXT, movie_id TEXT, rating TEXT, exported INTEGER, PRIMARY KEY(user_id, movie_id));"

_INSERT_MOVIE_ = "INSERT OR IGNORE INTO movie(movie_id, name, year, runtime, genre, rating, image_link) VALUES(?,?,?,?,?,?,?);"
_INSERT_USER_RATING_ = "INSERT OR IGNORE INTO user_ratings(user_id, movie_id, rating, exported) VALUES(?,?,?,?);"
_INSERT_USER_ = "INSERT OR IGNORE INTO user(user_id, name) values(?,?);"

_EXISTS_USER_RATING_ = "SELECT EXISTS(SELECT 1 FROM user_ratings u where u.user_id = ? and u.movie_id = ? LIMIT 1);"
_SELECT_NEW_USER_RATING_ = "SELECT u.user_id, u.movie_id, u.rating, m.name, m.year, m.runtime, m.genre, m.rating as avg_rating, m.image_link from user_ratings u, movie m where u.user_id = ? and u.exported = 0 and u.movie_id = m.movie_id order by u.rowid;"
_SELECT_USER_ = "SELECT u.user_id, u.name from user u;"

_SELECT_NEW_MOVIES_ = "SELECT ur.movie_id, m.name, m.year, m.runtime, m.genre, m.rating, m.image_link  FROM user_ratings ur, movie m WHERE ur.exported = 0 and ur.movie_id = m.movie_id order by ur.rowid;"
_SELECT_COMPARE_RATINGS_ = "SELECT u.user_id, u.rating, us.name from user_ratings u, user us WHERE u.movie_id = ? and u.user_id = us.user_id;"

_UPDATE_EXPORTED_ = "UPDATE user_ratings set exported = 1 where exported = 0;"

_FEED_CONTENT_ = """<a href="{link}">
    <img
        alt="{name} {year}"
        height="209"
        src="{image_link}"
        width="140"
    >
</a>

<h3>
    <a href="{link}"> {name} {year} {user_rating}</a>
</h3>

<p>{runtime}</p>

<p>{genre}</p>

<p>{avg_rating}</p>"""

_FEED_COMPARE_CONTENT_ = """<a href="{link}">
    <img
        alt="{name} {year}"
        height="209"
        src="{image_link}"
        width="140"
    >
</a>

<h3>
    <a href="{link}"> {name} {year} </a>
</h3>

<p>"Runtime: " {runtime} </p>

<p>"Genre: "{genre} </p>

<p>"Avg. Rating: " {avg_rating} </p> """


def _get_page(userid, pagination_key=None):
    logging.debug("request ratings for userid: " + userid)
    url = "http://www.imdb.com/user/" + userid + "/ratings"
    parameters = {'sort': "date_added", 'mode': "detail"}
    if pagination_key is not None:
        parameters['paginationKey'] = pagination_key
    page = requests.get(url, params=parameters)
    return page


def _open_conn(db_location):
    conn = sqlite3.connect(db_location)
    return conn


def _close_conn(conn):
    conn.commit()
    conn.close()


def _initdb(db_location, users):
    logging.debug("Init db: " + db_location)
    conn = _open_conn(db_location)

    try:
        conn.execute(_CREATE_TABLE_MOVIE_)
        conn.execute(_CREATE_TABLE_USER_)
        conn.execute(_CREATE_TABLE_USER_RATINGS_)

        for user in users:
            logging.debug("Inserting user {0}, {1}".format(
                user['id'], user['name']))
            conn.execute(_INSERT_USER_, (user['id'], user['name']))

    except sqlite3.OperationalError as err:
        logging.error("Operational Error", err)

    conn.commit()
    _close_conn(conn)


def _get_url_param(url, param):
    """
        parses given url using urlparse module and returns the value of requested param.
    """
    params = parse_qs(urlparse(url).query)
    return params[param][0]


def _insert_movies(conn, movies):
    for movie in movies:
        conn.execute(_INSERT_MOVIE_, (movie['id'], movie['name'], movie['year'], movie[
                     'runtime'], movie['genre'], movie['rating'], movie['image_link']))
    conn.commit()


def _insert_user_ratings(conn, user_ratings):
    for rating in reversed(user_ratings):
        param = (rating['user_id'], rating['movie_id'], rating['rating'], 0)
        try:
            conn.execute(_INSERT_USER_RATING_, param)
        except sqlite3.InterfaceError as err:
            logging.error("Operational Error", err)
            print(rating['rating'])
            break
    conn.commit()


def _parse_ratings(userid, conn=None):
    movies = []
    user_ratings = []
    pagination_key = None
    continue_fetch = True
    while continue_fetch:
        page = _get_page(userid, pagination_key)
        if page.status_code == requests.codes.ok:
            soup = BeautifulSoup(page.text, 'html.parser')
            ratings_div = soup.find("div", id="ratings-container")

            for rating_div in ratings_div.find_all("div", class_="lister-item"):
                image_div = rating_div.find("div", class_="lister-item-image")
                image_link = None
                if image_div:
                    img = image_div.find("img")
                    if img:
                        image_link = img['loadlate']

                item_div = rating_div.find("div", class_="lister-item-content")
                header_h3 = item_div.find("h3", class_="lister-item-header")

                index = None
                index_span = header_h3.find("span", class_="lister-item-index")
                if index_span:
                    index = index_span.get_text()

                movie_name = None
                movie_id = None
                for header_link in header_h3.find_all("a"):
                    movie_name = header_link.text
                    movie_id = header_link['href'].replace(
                        "/title/", "").replace("/?ref_=rt_li_tt", "")

                year = None
                year_span = item_div.find("span", class_="lister-item-year")
                if year_span:
                    year = year_span.get_text(strip=True)

                runtime = None
                runtime_span = item_div.find("span", class_="runtime")
                if runtime_span:
                    runtime = runtime_span.get_text(strip=True)

                genre = None
                genre_span = item_div.find("span", class_="genre")
                if genre_span:
                    genre = genre_span.get_text(strip=True)

                rating_widget_div = item_div.find(
                    "div", class_="ipl-rating-widget")
                rating = None
                rating_div = rating_widget_div.find(
                    "div", class_="ipl-rating-star")
                if rating_div:
                    rating_span = rating_div.find(
                        "span", class_="ipl-rating-star__rating")
                    if rating_span:
                        rating = rating_span.get_text()

                user_rating = None
                user_rating_div = rating_widget_div.find(
                    "div", class_="ipl-rating-star--other-user")
                if user_rating_div:
                    user_rating_span = user_rating_div.find(
                        "span", class_="ipl-rating-star__rating")
                    if user_rating_span:
                        user_rating = user_rating_span.get_text()

                new_rating = True
                if conn:
                    c = conn.execute(_EXISTS_USER_RATING_, (userid, movie_id))
                    if c.fetchone()[0] == 1:
                        new_rating = False

                if new_rating:
                    movie = {}
                    movie['id'] = movie_id
                    movie['name'] = movie_name
                    movie['year'] = year
                    movie['runtime'] = runtime
                    movie['genre'] = genre
                    movie['rating'] = rating
                    movie['image_link'] = image_link
                    movies.append(movie)

                    user_movie = {}
                    user_movie['user_id'] = userid
                    user_movie['movie_id'] = movie_id
                    user_movie['rating'] = user_rating
                    user_ratings.append(user_movie)
                else:
                    continue_fetch = False
                    break

            if continue_fetch:
                pagination_key = None
                pagination_div = ratings_div.find(
                    "div", class_="list-pagination")
                if pagination_div:
                    next_page_link = pagination_div.find(
                        "a", class_="next-page")
                    if next_page_link:
                        pagination_key = _get_url_param(
                            next_page_link['href'], "paginationKey")

                if pagination_key is None:
                    continue_fetch = False
        else:
            logging.error("Request status code is: " + page.status_code)
            break
    # while continue_fetch
    return movies, user_ratings


def _create_feed(conn, user, folder_path):
    user_id = user['user_id']
    user_name = user['user_name']
    fg = FeedGenerator()
    fg.id("http://www.imdb.com/user/{0}/ratings".format(user_id))
    fg.title("{0}'s imdb ratings".format(user_name))
    fg.link(
        href="http://www.imdb.com/user/{0}/ratings".format(user_id), rel='related')
    fg.description("{0}'s imdb ratings".format(user_name))
    fg.author({'name': user_name, 'email': "{0}@fuzzywave.com".format(user_name)})

    for row in conn.execute(_SELECT_NEW_USER_RATING_, (user_id,)):
        movie_link = "http://www.imdb.com/title/{0}".format(row[1])
        m_dict = {'id': row[1], 'link': movie_link, 'name': row[3], 'user_rating': row[2], 'year': row[
            4], 'runtime': row[5], 'genre': row[6], 'avg_rating': row[7], 'image_link': row[8]}

        fe = fg.add_entry()
        fe.id(m_dict['link'])
        fe.title("{name} {year} - {user_rating}".format_map(m_dict))
        fe.link(href=m_dict['link'], rel='related')
        fe.content(_FEED_CONTENT_.format_map(m_dict), type="html")

    pathlib.Path(folder_path + "/" +
                 user_id).mkdir(parents=True, exist_ok=True)
    fg.atom_file(folder_path + "/" + user_id + "/ratings_atom.xml")
    fg.rss_file(folder_path + "/" + user_id + "/ratings_rss.xml")


def _create_compare_feed(conn, folder_path):
    fg = FeedGenerator()
    fg.id("http://www.imdb.com")
    fg.title("Ratings Comparison")
    fg.link(href="http://www.imdb.com", rel='related')
    fg.description("Ratings comparison")
    fg.author({'name': "y", 'email': "y@fuzzywave.com"})

    for movie in conn.execute(_SELECT_NEW_MOVIES_):
        movie_id = movie[0]
        movie_link = "http://www.imdb.com/title/{0}".format(movie_id)
        m_dict = {'id': movie_id, 'link': movie_link, 'name': movie[1],
                  'year': movie[2], 'runtime': movie[3], 'genre': movie[4], 'avg_rating': movie[5], 'image_link': movie[6]}

        fe = fg.add_entry()
        fe.id(m_dict['link'])
        fe.title("{name} {year}".format_map(m_dict))
        fe.link(href=m_dict['link'], rel='related')
        content = _FEED_COMPARE_CONTENT_.format_map(m_dict)
        for row in conn.execute(_SELECT_COMPARE_RATINGS_, (movie_id,)):
            content += "<p>" + "{0}: {1}".format(row[2], row[1]) + "</p>"

        fe.content(content, type="html")

    pathlib.Path(folder_path).mkdir(parents=True, exist_ok=True)
    fg.atom_file(folder_path + "/ratings_atom.xml")
    fg.rss_file(folder_path + "/ratings_rss.xml")


def _parse_userfile(userfile):
    users = []
    with open(userfile) as f:
        for line in f:
            user = {}
            userid, username = line.rstrip('\n').split(',')
            user['id'] = userid
            user['name'] = username
            users.append(user)
    return users


def _get_users(conn):
    users = []
    for row in conn.execute(_SELECT_USER_):
        user = {'user_id': row[0], 'user_name': row[1]}
        users.append(user)
    return users


def _mark_exported(conn):
    conn.execute(_UPDATE_EXPORTED_)
    conn.commit()


def main():
    parser = argparse.ArgumentParser(description='IMDB Ratings RSS.')
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity", action="store_true")
    parser.add_argument("-i", "--input", help="sqlite database file path")
    parser.add_argument("-o", "--output", help="rss feed output folder path")
    parser.add_argument("-u", "--userfile", help="userfile path")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    users = _parse_userfile(args.userfile)
    _initdb(args.input, users)

    conn = _open_conn(args.input)
    has_new_entry = False
    for user in _get_users(conn):
        movies, user_ratings = _parse_ratings(user['user_id'], conn)
        logging.info("{0} - Username:{1} - New ratings:{2}".format(
            user['user_id'], user['user_name'], len(user_ratings)))
        if len(user_ratings) > 0:
            _insert_movies(conn, movies)
            _insert_user_ratings(conn, user_ratings)
            _create_feed(conn, user, args.output)
            has_new_entry = True
    if has_new_entry:
        _create_compare_feed(conn, args.output)
        _mark_exported(conn)

    logging.info("Finished successfully at {0}".format(
        datetime.now().isoformat()))

if __name__ == '__main__':
    main()
